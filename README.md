# Real Estate Application

## Description

The Real Estate Application is a web application where real estate employees can register, log in, and record their appointments dates and lengths. The manager will be able to follow all the stages thanks to these records. Laravel is used in the back end. On the front end side, Tailwind CSS, developed in React and Figma, is used.

## Technologies

- Laravel
- MySQL
- React
- Tailwind CSS

## Contributor

- Creator of application @aaktepe